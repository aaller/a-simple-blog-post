---
title: "Devops:Understanding general concepts"
date: 2019-03-02T20:28:41+01:00
draft: false
tags: ["Getting started"]
---

Let's start with some general concepts of Devops

     What is systems administration or systems engineering? 

     What is DevOps?

         Collaboration and trust.

         A revolution in tools for systems engineering.

         Embracing risk and velocity.

         Align with business objectives.

         Servicing your teams.

        What is SRE. 

     What DevOps is not.

     Software maturity cycle.

     Introduction to the 3 ways of DevOps. 

     Git workflow.

     Pipeline based development.




